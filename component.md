# Components
### Non-Invasive Current Sensor - (100A Max)
Non-Invasive Current Sensor - (100 A Max) เป็น Sensor วัดกระแส ใช้งานลักษณะ Clamp (ครอบสายไฟ) สามารถวัดกระแสได้สูงสุดถึง 100 A (Current Output) 

R1 เเละ R2 เเละ output เเรงดันสูงสุดที่ 1 โวลล์ สำหรับ AC adapter 9V RMS output ใช้ตัวต้านทาน R1(10k) เเละ R2(100k) ก็จะได้ค่า output ที่เหมาะสม

```text
peak-voltage-output = R1 / (R1 + R2) x peak-voltage-input = 10k / (10k + 100k) x 12.7V = 1.15V
```


### Calculating a suitable burden resistor size
ถ้า CT sensor กระเเสที่ได้อย่างเช่น YHDC SCT-013-000 เราจะต้องเปลี่ยนสัญญาณที่เป็นกระเเสไฟฟ้า เป็นสัญญาณที่เป็นเเรงดัน โดยวิธี burden resistor เเต่ถ้าสัญญาณที่เป็น output เป็นเเรงดัน เราไม่จำเป็นต้อง burden resistor เพราะมันอยู่ในตัว CT เเล้ว



!! เนื้อหาเเปลจากอังกฤษ เเละเนื้อหาจากภาษาไทยด้วย อาจมีความคลาดเคลื่อนหรือเนื้อหากำกวม ผู้เขียนเป็นเเค่มือสมัครเล่น