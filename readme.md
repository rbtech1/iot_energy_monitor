Energy moritor

  - กรณีต้องลดค่าใช้จ่าย โดยลิมิตค่าใช้จ่ายไว้ที่ 1000฿ สำหรับโปรเจคนี้ ดังนั้น เมื่อต้องการใช้ software opensource ่grafana, influxDB อาจจะใช้ cloud service (จะมีค่าใช้จ่ายสำหรับการใช้งานระยะยาวหรือเกินอัตราการใช้งานฟรีของเเต่ละ service) or local server สำหรับกรณีที่ไม่ต้องการความยุ่งยากสำหรับเเสดงผลเป็นกราฟ โดย container จะมีส่วนช่วยให้ทำงานในส่วนเเสดงผลได้ง่ายมากขึ้น ในกรณีของผมเอง เป็นเเค่ development ไม่ได้ใช้งานใน prodcution ผมจะใช้คอมพิวเตอร์ที่สามารถใช้ unix (xubuntu) เเล้วติดตั้ง docker เพื่อใช้งานสำหรับ container สำหรับเเสดงผลครับ

### Components
```txt
- [200฿] ESP32-WROOM-32               
- [190฿] x1 CT sensor SCT-013-000 Model 100A/50mA
- [160฿] Sitronix ST7735 LCD
- [xxx฿] Box case own design
```

### Ref
https://www.mcielectronics.cl/website_MCI/static/documents/Datasheet_SCT013.pdf
https://www.displayfuture.com/Display/datasheet/controller/ST7735.pdf


!! เนื้อหาเเปลจากอังกฤษ เเละเนื้อหาจากภาษาไทยด้วย อาจมีความคลาดเคลื่อนหรือเนื้อหากำกวม ผู้เขียนเป็นเเค่มือสมัครเล่น