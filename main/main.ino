#include <EmonLib.h>

EnergyMonitor g_EMor;  // instance valiable

void setup() {
  // put your setup code here, to run once:
  Serail.begin(9600);

  g_EMor.voltage(2, 234.26, 1.7);
  g_EMor.current(1, 111.1);
}

void loop() {
  // put your main code here, to run repeatedly:

  g_EMor.calcVI(20, 2000);
  g_EMor.serialprint();

  float realPower       = g_EMor.realPower();
  float apparentPower   = g_EMor.apparentPower();
  float powerFacotr     = g_EMor.powerFactor();
  float supplyVoltage   = g_EMor.supplyVoltage();
  float IRMS            = g_EMor.IRMS();
}
