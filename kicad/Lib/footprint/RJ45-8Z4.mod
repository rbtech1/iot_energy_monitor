PCBNEW-LibModule-V1  2022-07-25 01:01:01
# encoding utf-8
Units mm
$INDEX
RJ458Z4
$EndINDEX
$MODULE RJ458Z4
Po 0 0 0 15 62dddd3d 00000000 ~~
Li RJ458Z4
Cd RJ45-8Z4-1
Kw Connector
Sc 0
At STD
AR 
Op 0 0 0
T0 -4.075 4.445 1.27 1.27 0 0.254 N V 21 N "J**"
T1 -4.075 4.445 1.27 1.27 0 0.254 N I 21 N "RJ458Z4"
DS -14.23 12.445 4.57 12.445 0.2 24
DS 4.57 12.445 4.57 -3.555 0.2 24
DS 4.57 -3.555 -14.23 -3.555 0.2 24
DS -14.23 -3.555 -14.23 12.445 0.2 24
DS 4.57 12.445 -5.25 12.445 0.1 21
DS -5.25 12.445 -5.25 12.445 0.1 21
DS -5.25 12.445 4.57 12.445 0.1 21
DS 4.57 12.445 4.57 12.445 0.1 21
DS -12 12.445 -14.25 12.445 0.1 21
DS -14.25 12.445 -14.25 12.445 0.1 21
DS -14.25 12.445 -12 12.445 0.1 21
DS -12 12.445 -12 12.445 0.1 21
DS -14.25 12.445 -14.23 12.445 0.1 21
DS -14.23 12.445 -14.23 -3.555 0.1 21
DS -14.23 -3.555 -14.25 -3.555 0.1 21
DS -14.25 -3.555 -14.25 12.445 0.1 21
DS -14.23 -3.555 -11.5 -3.555 0.1 21
DS -11.5 -3.555 -11.5 -3.555 0.1 21
DS -11.5 -3.555 -14.23 -3.555 0.1 21
DS -14.23 -3.555 -14.23 -3.555 0.1 21
DS -7 -3.555 4.57 -3.555 0.1 21
DS 4.57 -3.555 4.57 -3.555 0.1 21
DS 4.57 -3.555 -7 -3.555 0.1 21
DS -7 -3.555 -7 -3.555 0.1 21
DS -15.25 -6.188 7.1 -6.188 0.1 24
DS 7.1 -6.188 7.1 15.078 0.1 24
DS 7.1 15.078 -15.25 15.078 0.1 24
DS -15.25 15.078 -15.25 -6.188 0.1 24
DS 4.57 -3.555 4.57 -3.555 0.1 21
DS 4.57 -3.555 4.57 12.445 0.1 21
DS 4.57 12.445 4.57 12.445 0.1 21
DS 4.57 12.445 4.57 -3.555 0.1 21
DS 6 -0 6 -0 0.2 21
DS 6.1 -0 6.1 -0 0.2 21
DS 6 -0 6 -0 0.2 21
DA 6.05 -0 6.000 -0 -1800 0.2 21
DA 6.05 -0 6.100 -0 -1800 0.2 21
DA 6.05 -0 6.000 -0 -1800 0.2 21
$PAD
Po 0.000 -0
Sh "1" C 1.493 1.493 0 0 900
Dr 0.96 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 2.540 1.27
Sh "2" C 1.493 1.493 0 0 900
Dr 0.96 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 2.54
Sh "3" C 1.493 1.493 0 0 900
Dr 0.96 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 2.540 3.81
Sh "4" C 1.493 1.493 0 0 900
Dr 0.96 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 5.08
Sh "5" C 1.493 1.493 0 0 900
Dr 0.96 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 2.540 6.35
Sh "6" C 1.493 1.493 0 0 900
Dr 0.96 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0.000 7.62
Sh "7" C 1.493 1.493 0 0 900
Dr 0.96 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 2.540 8.89
Sh "8" C 1.493 1.493 0 0 900
Dr 0.96 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -9.400 -3.68
Sh "MH1" C 3.015 3.015 0 0 900
Dr 2.01 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -9.400 12.57
Sh "MH2" C 3.015 3.015 0 0 900
Dr 2.01 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -6.350 -1.27
Sh "MH3" C 1.665 1.665 0 0 900
Dr 3.33 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -6.350 10.16
Sh "MH4" C 1.665 1.665 0 0 900
Dr 3.33 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE RJ458Z4
$EndLIBRARY
